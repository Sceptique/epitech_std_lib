/*
** my_strsplit.c for  in /home/poulet_a/projets/minishell1/src
** 
** Made by poulet_a
** Login   <poulet_a@epitech.eu>
** 
** Started on  Wed Dec 18 09:05:09 2013 poulet_a
** Last update Fri Mar  7 09:01:21 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"

int		my_ss_count(char *str, const char sep)
{
  int		i;
  int		n;
  char		backslash;

  RET_NULL_ZERO(str);
  RET_ZERO_ZERO(str[0]);
  n = 1;
  TWO_ZERO(i, backslash);
  while (str[i])
    {
      if (backslash == 0)
	{
	  if (str[i] == sep && str[i + 1] && str[i + 1] != sep)
	    n++;
	  if (str[i] == '\\')
	    backslash = 1;
	}
      else
	backslash = 0;
      i++;
    }
  return (n + 1);
}

void	my_strsplit_writer(char **out, char **str_cpy, int *i, int *n)
{
  (*str_cpy)[*i] = 0;
  out[(*n)++] = *str_cpy + *i + 1;
  (*str_cpy)[*i] = 0;
}

char		**my_strsplit(char *str, const char sep)
{
  char		**out;
  char		*str_cpy;
  int		i;
  int		n;
  char		backslash;

  RET_NULL_NULL((str_cpy = my_strdup(str)));
  RET_NULL_NULL((out = malloc(sizeof(char*) * (my_ss_count(str, sep) + 2))));
  TWO_ZERO(i, backslash);
  n = 1;
  out[0] = str_cpy;
  while (str[i])
    {
      if (backslash == 0)
	{
	  if (str[i] == sep && str[i + 1] && str[i + 1] != sep)
	    my_strsplit_writer(out, &str_cpy, &i, &n);
	  backslash = (str[i] == '\\') ? (1) : (0);
	}
      else
	backslash = 0;
      i++;
    }
  out[n] = 0;
  return (out);
}

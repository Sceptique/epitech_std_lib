/*
** my_list_remove.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Fri Feb  7 19:08:19 2014 poulet_a
** Last update Fri Feb  7 19:15:52 2014 poulet_a
*/

#include "my.h"

/*
** be carefull with prev. we may use a safer way...
*** list = NULL ... ? sure ?
*/
int		list_remove(t_list *list)
{
  t_list	*prev;
  t_list	*next;

  RET_NULL_LONE(list);
  next = list->next;
  prev = list->prev;
  if (prev != NULL)
    prev->next = next;
  if (next != NULL)
    next->prev = prev;
  free(list);
  list = NULL;
  return (0);
}

int		list_delete_at(t_list *list, int id)
{
  t_list	*tmp;

  RET_NULL_LONE((tmp = list_index(list, id)));
  return (list_remove(tmp));
}
